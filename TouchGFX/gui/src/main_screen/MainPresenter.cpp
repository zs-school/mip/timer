#include <gui/main_screen/MainView.hpp>
#include <gui/main_screen/MainPresenter.hpp>

MainPresenter::MainPresenter(MainView& v)
    : view(v)
{
}

void MainPresenter::activate()
{
}

void MainPresenter::deactivate()
{
}

void MainPresenter::setBackup(uint32_t value)
{
    model->setBackup(value);
}

uint32_t MainPresenter::getBackup() const
{
    return model->getBackup();
}