#include <gui/main_screen/MainView.hpp>
#include "BitmapDatabase.hpp"
#include <texts/TextKeysAndLanguages.hpp>

constexpr uint8_t UPPER_LIMIT = 60;
constexpr uint8_t LOWER_LIMIT = 0;

void MainView::setupScreen()
{
    mytime = presenter->getBackup();
    updateGFXElements();
}

void MainView::handleTickEvent()
{
    count++;

    if (count == 60)
    {
        switch (state)
        {
            case State::Run:
                if (mytime > 0)
                {
                    mytime--;
                    updateGFXElements();
                }

                if (mytime == 0)
                {
                    state = State::Boom;
                }
                break;

            default:
                break;

        }
        count = 0;      
    }

    if (state == State::Boom)
    {        
        application().gotoScreen1ScreenNoTransition();
    }
}

void MainView::increaseMinValue()
{
    if (mytime <= 59*UPPER_LIMIT)
    {
        mytime += 60;
    }
    presenter->setBackup(mytime);
    updateGFXElements();
}

void MainView::increaseSecValue()
{
    if (mytime <= 3599)
    {
        mytime += 1;
    }
    presenter->setBackup(mytime);
    updateGFXElements();
}

void MainView::decreaseMinValue()
{
    mytime -= 60;
    presenter->setBackup(mytime);
    updateGFXElements();
}

void MainView::decreaseSecValue()
{
    mytime -= 1;
    presenter->setBackup(mytime);
    updateGFXElements();
}

void MainView::reset()
{
    state = State::Stop;
    mytime = presenter->getBackup();
    updateGFXElements();
}

void MainView::start()
{
    if (state == State::Stop)
    {
        state = State::Run;
    }
    else 
    {
        state = State::Stop;
    }
    updateGFXElements();
}

void MainView::updateGFXElements()
{
    //Counter text area GFX uptade.
    // Unicode::snprintf(countTxtBuffer, 3, "%d", count);

    Unicode::snprintf(minTxtBuffer, 3, "%02d", mytime / 60);
    Unicode::snprintf(secTxtBuffer, 3, "%02d", mytime % 60);

    //Button GFX update and touchable.
    switch (state)
    {
        case State::Run:
            buttonMinUp.setTouchable(false);
            buttonMinDown.setTouchable(false);
            buttonSecUp.setTouchable(false);
            buttonSecDown.setTouchable(false);

            buttonMinUp.setBitmaps(Bitmap(BITMAP_UP_BTN_DISABLED_ID), Bitmap(BITMAP_UP_BTN_DISABLED_ID));
            buttonMinDown.setBitmaps(Bitmap(BITMAP_DOWN_BTN_DISABLED_ID), Bitmap(BITMAP_DOWN_BTN_DISABLED_ID));
            buttonSecUp.setBitmaps(Bitmap(BITMAP_UP_BTN_DISABLED_ID), Bitmap(BITMAP_UP_BTN_DISABLED_ID));
            buttonSecDown.setBitmaps(Bitmap(BITMAP_DOWN_BTN_DISABLED_ID), Bitmap(BITMAP_DOWN_BTN_DISABLED_ID));
            buttonStart.setLabelText(TypedText(T_RESOURCEID2));
            break;
        case State::Stop:

            buttonStart.setLabelText(TypedText(T_RESOURCEID1));
            if (mytime >= 3600)
            {
                buttonSecUp.setTouchable(false);
                buttonSecUp.setBitmaps(Bitmap(BITMAP_UP_BTN_DISABLED_ID), Bitmap(BITMAP_UP_BTN_DISABLED_ID));
            }
            else
            {
                buttonSecUp.setTouchable(true);
                buttonSecUp.setBitmaps(Bitmap(BITMAP_UP_BTN_ID), Bitmap(BITMAP_UP_BTN_PRESSED_ID));
            }

            if (mytime >= 59*UPPER_LIMIT + 1)
            {
                buttonMinUp.setTouchable(false);
                buttonMinUp.setBitmaps(Bitmap(BITMAP_UP_BTN_DISABLED_ID), Bitmap(BITMAP_UP_BTN_DISABLED_ID));
            }
            else
            {
                buttonMinUp.setTouchable(true);
                buttonMinUp.setBitmaps(Bitmap(BITMAP_UP_BTN_ID), Bitmap(BITMAP_UP_BTN_PRESSED_ID));
            }

            if (mytime == 0)
            {
                buttonSecDown.setTouchable(false);
                buttonSecDown.setBitmaps(Bitmap(BITMAP_DOWN_BTN_DISABLED_ID), Bitmap(BITMAP_DOWN_BTN_DISABLED_ID));   
            }
            else
            {
                buttonSecDown.setTouchable(true);
                buttonSecDown.setBitmaps(Bitmap(BITMAP_DOWN_BTN_ID), Bitmap(BITMAP_DOWN_BTN_PRESSED_ID));   
            }

            if (mytime < 60)
            {
                buttonMinDown.setTouchable(false);
                buttonMinDown.setBitmaps(Bitmap(BITMAP_DOWN_BTN_DISABLED_ID), Bitmap(BITMAP_DOWN_BTN_DISABLED_ID));   
            }
            else
            {
                buttonMinDown.setTouchable(true);
                buttonMinDown.setBitmaps(Bitmap(BITMAP_DOWN_BTN_ID), Bitmap(BITMAP_DOWN_BTN_PRESSED_ID));   
            }
            break;

        default:
            break;
    }

    // Invalidate all GFX area, which will result in it being redrawn in next tick.
    // countTxt.invalidate();
    minTxt.invalidate();
    secTxt.invalidate();
    buttonMinUp.invalidate();
    buttonMinDown.invalidate();
    buttonSecUp.invalidate();
    buttonSecDown.invalidate();
    buttonStart.invalidate();
    // buttonUp.invalidate();
    // buttonDown.invalidate();
}
