#include <gui/model/Model.hpp>
#include <gui/model/ModelListener.hpp>

Model::Model() : modelListener(0)
{
}

void Model::tick()
{
}

void Model::setBackup(uint32_t value)
{
	backup = value;
}

uint32_t Model::getBackup() const
{
	return backup;
}