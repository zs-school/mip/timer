#ifndef MAIN_VIEW_HPP
#define MAIN_VIEW_HPP

#include <gui_generated/main_screen/MainViewBase.hpp>
#include <gui/main_screen/MainPresenter.hpp>

class MainView : public MainViewBase
{
public:
    MainView() = default;

    virtual void setupScreen() override;
    virtual void handleTickEvent() override;

    virtual void increaseMinValue() override;
    virtual void increaseSecValue() override;
    virtual void decreaseMinValue() override;
    virtual void decreaseSecValue() override;
    virtual void reset() override;
    virtual void start() override;

    void updateGFXElements();
    enum class State {Run, Stop, Boom, State_END};
protected:

private:
    uint8_t count = 0;
    uint32_t mytime = 0;
    State state = State::Stop;
};

#endif // MAIN_VIEW_HPP
